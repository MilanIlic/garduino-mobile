import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:garduino_mobile/page/home/home.dart';
import 'package:garduino_mobile/page/settings/plant_system_config.dart';
import 'package:garduino_mobile/util/app_localization.dart';
import 'inherited_data_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return InheritedDataProvider(
      child: MaterialApp(
        title: 'Garduino Mobile',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          AppLocalizationsDelegate.delegate
        ],
        supportedLocales: [
          Locale("en", "US"),
          Locale("sr")
        ],
        home: MyHomePage(),
        routes: {
          "/settings": (context) => PlantSystemConfig(),
        },
      ),
    );
  }
}
