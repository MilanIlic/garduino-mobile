import 'package:flutter/cupertino.dart';
import 'package:garduino_mobile/util/public_extension.dart';

class SystemConfigModel {
  bool autoWateringEnabled;
  bool lightOnDuringWatering;
  bool telegramNotifierEnabled;

  String scrapeInterval;
  String numOfScrapes;
  String moistureThreshold;
  String temperatureThreshold;
  String humidityThreshold;

  SystemConfigModel(
      {this.autoWateringEnabled,
      this.lightOnDuringWatering,
      this.telegramNotifierEnabled,
      this.scrapeInterval,
      this.numOfScrapes,
      this.moistureThreshold,
      this.temperatureThreshold,
      this.humidityThreshold});

  SystemConfigModel.fromJson(Map<String, dynamic> map) {
    autoWateringEnabled =
        map["auto_watering"].toString().toLowerCase().startsWith("t");
    lightOnDuringWatering = map["light_on_during_watering"]
        .toString()
        .toLowerCase()
        .startsWith("t");
    telegramNotifierEnabled = map["telegram_notifier_enabled"]
        .toString()
        .toLowerCase()
        .startsWith("t");
    scrapeInterval = parseScrapeIntervalFromIntSeconds(
        int.parse(map["watering_scrape_interval"]));
    numOfScrapes = map["watering_num_of_scrapes"].toString();
    moistureThreshold = map["watering_moisture_threshold"].toString();
    temperatureThreshold = map["watering_temperature_threshold"].toString();
    humidityThreshold = map["watering_humidity_threshold"].toString();
  }

  Map<String, String> toMap() {
    return {
      "auto_watering": autoWateringEnabled.toString(),
      "light_on_during_watering": lightOnDuringWatering.toString(),
      "telegram_notifier_enabled": telegramNotifierEnabled.toString(),
      "watering_scrape_interval": parseScrapeIntervalInSeconds().toString(),
      "watering_num_of_scrapes": numOfScrapes,
      "watering_moisture_threshold": moistureThreshold,
      "watering_temperature_threshold": temperatureThreshold,
      "watering_humidity_threshold": humidityThreshold,
    };
  }

  @override
  String toString() {
    String result = "Config: [numOfScrapes = $numOfScrapes];";
    return result;
  }

  String getScrapeIntervalLocalized(BuildContext context) {
    var parts = scrapeInterval.split(" ");

    var numValue = parts[0];
    var unit = parts[1];

    if (unit == "sec") {
      return numValue + " " + "second".tr(context);
    } else if (unit == "min") {
      return numValue + " " + "minute".tr(context);
    }

    return null;
  }

  String parseScrapeIntervalFromIntSeconds(int seconds) {
    if (seconds < 60)
      return seconds.toString() + " sec";
    else
      return (seconds / 60).floor().toString() + " min";
  }

  int parseScrapeIntervalInSeconds() {
    var parts = scrapeInterval.split(" ");

    int numValue = int.parse(parts.elementAt(0));
    String metricValue = parts.elementAt(1);

    if (metricValue == "sec")
      return numValue;
    else if (metricValue == "min") return numValue * 60;

    print("Error while parsing scrape interval value.");
    return numValue;
  }
}
