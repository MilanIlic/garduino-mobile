

import 'package:intl/intl.dart';

class PlantStatusModel {

  String airTemperature;
  String airHumidity;
  String soilMoisture;
  String timestamp;
  String wateringDoneAt;

  PlantStatusModel({this.airTemperature, this.airHumidity, this.soilMoisture, timestampStr, wateringDoneAtStr}) {
    if (timestampStr != null && timestampStr != "unknown")
      timestamp = parseLocalFromUtcDateTime(timestampStr.substring(0, 19));
    else
      timestamp = "unknown";

    if (wateringDoneAtStr != null && wateringDoneAtStr != "unknown")
      wateringDoneAt = parseLocalFromUtcDateTime(wateringDoneAtStr.substring(0, 19));
    else
      wateringDoneAt = "unknown";

    // remove digits after decimal point
    if (airTemperature != "unknown")
      this.airTemperature = airTemperature.substring(0, airTemperature.indexOf("."));
    if (airHumidity != "unknown")
      this.airHumidity = airHumidity.substring(0, airHumidity.indexOf("."));
    if (soilMoisture != "unknown")
      this.soilMoisture = soilMoisture.substring(0, soilMoisture.indexOf("."));
  }

  String parseLocalFromUtcDateTime(String timestamp) {
    DateFormat format = new DateFormat("yyyy-MM-dd HH:mm:ss");
    DateTime localTime = format.parseUTC(timestamp).toLocal();

    DateFormat newFormat = new DateFormat("dd/MM/yy HH:mm:ss");
    return newFormat.format(localTime);
  }

}