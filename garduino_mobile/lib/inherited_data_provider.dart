import 'package:flutter/material.dart';


class InheritedDataProvider extends InheritedWidget {
  String serverAddress = "192.168.102.166"; // "192.168.110.72";
  String serverPort = "8080";
  bool autoRefresh = false;
  bool mockedData = false;

  static const int maxWaitTimeSeconds = 5;

  InheritedDataProvider({Key key, Widget child}) : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedDataProvider oldWidget) {
    bool result = false;

    result |= oldWidget.serverAddress != serverAddress;
    result |= oldWidget.autoRefresh != autoRefresh;
    result |= oldWidget.mockedData != mockedData;

    return result;
  }

  static InheritedDataProvider of(BuildContext context){
    return context.inheritFromWidgetOfExactType(InheritedDataProvider);
  }
}
