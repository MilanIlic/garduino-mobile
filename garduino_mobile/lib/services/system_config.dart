import 'package:flutter/src/widgets/framework.dart';
import 'package:garduino_mobile/module/system_config.dart';
import 'package:garduino_mobile/repository/system_config.dart';

class SystemConfigService {
  SystemConfigRepository _systemConfigRepository = SystemConfigRepository();

  Future<SystemConfigModel> fetchConfig(BuildContext context) async {
    print("=====> Fetching config");
    return _systemConfigRepository.fetchConfig(context);
  }

  Future<Map<String, dynamic>> updateSystemConfig(BuildContext context, SystemConfigModel cachedConfigModel) {
    return _systemConfigRepository.updateSystemConfig(context, cachedConfigModel);
  }
}
