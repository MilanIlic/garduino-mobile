import 'package:flutter/src/widgets/framework.dart';
import 'package:garduino_mobile/module/water_plant.dart';
import 'package:garduino_mobile/repository/water_plant.dart';

class WaterPlantService {
  WaterPlantRepository _waterPlantRepository = WaterPlantRepository();

  Future<WaterPlantModel> waterPlant(BuildContext context) async {
    return _waterPlantRepository.waterPlant(context);
  }
}
