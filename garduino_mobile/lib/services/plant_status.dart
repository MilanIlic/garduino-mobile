import 'package:flutter/src/widgets/framework.dart';
import 'package:garduino_mobile/module/plant_status.dart';
import 'package:garduino_mobile/repository/plant_status.dart';

class PlantStatusService {
  PlantStatusRepository _plantStatusRepository = PlantStatusRepository();

  Future<PlantStatusModel> fetchStatus(BuildContext context) async {
    return _plantStatusRepository.fetchStatus(context);
  }
}
