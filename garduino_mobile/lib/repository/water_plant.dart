import 'package:flutter/src/widgets/framework.dart';
import 'package:garduino_mobile/api/water_plant.dart';
import 'package:garduino_mobile/module/water_plant.dart';

class WaterPlantRepository {
  WaterPlantApi _waterPlantApi = WaterPlantApi();

  Future<WaterPlantModel> waterPlant(BuildContext context) async {
    var response = await _waterPlantApi.waterPlant(context);
    return WaterPlantModel(message: response["message"] ?? "unkown");
  }
}
