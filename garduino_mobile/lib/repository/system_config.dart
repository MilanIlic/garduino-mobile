import 'dart:async';

import 'package:flutter/src/widgets/framework.dart';
import 'package:garduino_mobile/api/system_config.dart';
import 'package:garduino_mobile/module/system_config.dart';

class SystemConfigRepository {
  SystemConfigApi _systemConfigApi = SystemConfigApi();
  SystemConfigModel systemConfigModel;
  Completer<SystemConfigModel> completer;


  Future<SystemConfigModel> fetchConfig(BuildContext context) async {
    if (systemConfigModel != null) {
      print("Cached config");
      return systemConfigModel;
    } else {
      var value = await _systemConfigApi.fetchConfig(context);
      systemConfigModel = SystemConfigModel.fromJson(value);
      return systemConfigModel;
    }
  }

  void onData(Map<String, dynamic> value) {
    print("Data has arived!" + value.toString());

    systemConfigModel = SystemConfigModel.fromJson(value);

    completer.complete(systemConfigModel);
  }

  Future<Map<String, dynamic>> updateSystemConfig(BuildContext context, SystemConfigModel newConfigModel) {
    systemConfigModel = newConfigModel;
    return _systemConfigApi.updateSystemConfig(context, newConfigModel.toMap());
  }
}