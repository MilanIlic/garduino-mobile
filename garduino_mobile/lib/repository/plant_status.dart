import 'package:flutter/src/widgets/framework.dart';
import 'package:garduino_mobile/api/plant_status.dart';
import 'package:garduino_mobile/module/plant_status.dart';

class PlantStatusRepository {
  PlantStatusApi _plantStatusApiHelper = PlantStatusApi();

  Future<PlantStatusModel> fetchStatus(BuildContext context) async {
    final Map<String, dynamic> response =
    await _plantStatusApiHelper.fetchStatus(context);

    return PlantStatusModel(
      airHumidity: response["air_humidity"] ?? "unknown",
      airTemperature: response["air_temperature"] ?? "unknown",
      soilMoisture: response["soil_moisture"] ?? "unknown",
      timestampStr: response["timestamp"] ?? null,
      wateringDoneAtStr: response["watering_done_at"] ?? null,
    );
  }
}