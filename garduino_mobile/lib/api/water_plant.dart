import 'package:http/http.dart' as http;
import 'package:flutter/src/widgets/framework.dart';
import 'api_base.dart';
import 'exceptions.dart';

class WaterPlantApi extends ApiBase {

  Future<Map<String, dynamic>> waterPlant(BuildContext context) async {
    try {
      updateConnectionParameters(context);

      var response = await http.get(Uri.http(serverAddress + ":" + serverPort, "/water-plant"));

      var responseJson = returnResponse(response);
      return responseJson;
    } catch (e) {
      throw FetchDataException("No Internet connection. " + e.toString());
    }
  }
}