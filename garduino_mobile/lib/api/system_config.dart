import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:flutter/src/widgets/framework.dart';
import '../inherited_data_provider.dart';
import 'api_base.dart';
import 'exceptions.dart';

class SystemConfigApi extends ApiBase {
  Future<Map<String, dynamic>> fetchConfig(BuildContext context) async {
    if (InheritedDataProvider.of(context).mockedData) {
      return fetchMockedConfig(context);
    }

    try {
      updateConnectionParameters(context);

      var response = await http
          .get(Uri.http(serverAddress + ":" + serverPort, "/config"))
          .timeout(Duration(seconds: maxWaitTimeSeconds), onTimeout: () {
        throw FetchDataException("Request timeout");
      });

      var responseJson = returnResponse(response);
      return responseJson;
    } catch (e) {
      throw FetchDataException("No Internet connection. " + e.toString());
    }
  }

  Future<Map<String, dynamic>> fetchMockedConfig(BuildContext context) async {

      var mockedBody = {
        "auto_watering": false,
        "light_on_during_watering": false,
        "telegram_notifier_enabled": false,
        "watering_scrape_interval": "2",
        "watering_num_of_scrapes": 6,
        "watering_moisture_threshold": 34,
        "watering_temperature_threshold": 35,
        "watering_humidity_threshold": 36,
      };

      var response = http.Response(json.encode(mockedBody), 200);
      return returnResponse(response);
  }

  Future<Map<String, dynamic>> updateSystemConfig(
      BuildContext context, Map<String, dynamic> newValues) async {
    if (InheritedDataProvider.of(context).mockedData) {
      return updateMockedSystemConfig(context, newValues);
    }

    try {
      updateConnectionParameters(context);

      var response = await http
          .put(Uri.http(serverAddress + ":" + serverPort, "/config"), body: newValues)
          .timeout(Duration(seconds: maxWaitTimeSeconds),
          onTimeout: () {
            throw FetchDataException("Request timeout");
          });

      var responseJson = returnResponse(response);
      return responseJson;
    } catch (e) {
      throw FetchDataException(e.toString());
    }
  }

  Future<Map<String, dynamic>> updateMockedSystemConfig(
      BuildContext context, Map<String, dynamic> newValues) async {
    var response = http.Response(json.encode(newValues), 200);
    return returnResponse(response);
  }
}
