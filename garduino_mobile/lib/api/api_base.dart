import 'package:http/http.dart' as http;
import 'dart:convert';
import '../inherited_data_provider.dart';
import 'exceptions.dart';
import 'package:flutter/src/widgets/framework.dart';

class ApiBase {

  String serverAddress;
  String serverPort;
  int maxWaitTimeSeconds;

  void updateConnectionParameters(BuildContext context) {
    serverAddress = InheritedDataProvider.of(context).serverAddress;
    serverPort = InheritedDataProvider.of(context).serverPort;
    maxWaitTimeSeconds = InheritedDataProvider.maxWaitTimeSeconds;
  }

  Map<String, dynamic> returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        return json.decode(response.body.toString());
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}