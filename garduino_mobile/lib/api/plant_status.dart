import 'package:garduino_mobile/inherited_data_provider.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/src/widgets/framework.dart';
import 'api_base.dart';
import 'exceptions.dart';
import 'dart:convert';

class PlantStatusApi extends ApiBase {

  Future<Map<String, dynamic>> fetchStatus(BuildContext context) async {
    if (InheritedDataProvider.of(context).mockedData) {
      return fetchMockedStatus(context);
    }

    try {
      updateConnectionParameters(context);

      final response = await http
          .get(Uri.http(serverAddress + ":" + serverPort, "/status"))
          .timeout(Duration(seconds: maxWaitTimeSeconds), onTimeout: () {
        throw FetchDataException("Request timeout");
      });

      var responseJson = returnResponse(response);
      return responseJson;
    } catch (e) {
      throw FetchDataException("No Internet connection. " + e.toString());
    }
  }

  Future<Map<String, dynamic>> fetchMockedStatus(BuildContext context) async {
      var mockedBody = {
        "air_humidity": "29.0",
        "air_temperature": "28.0",
        "soil_moisture": "30.0",
        "timestamp": "2020-01-22 20:20:05",
        "watering_done_at": "2020-01-22 20:13:56"
      };

      final response = http.Response(json.encode(mockedBody), 200);
      return returnResponse(response);
  }

}

