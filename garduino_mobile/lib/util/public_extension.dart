import 'package:flutter/material.dart';
import 'package:garduino_mobile/util/app_localization.dart';

extension StringTranslateExtension on String {
  tr(BuildContext context, {List<String> args}) =>
      AppLocalizations.of(context).translate(this);
}