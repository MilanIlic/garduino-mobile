import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;

class AppLocalizations {
  final Locale locale;
  AppLocalizations({this.locale, this.translationsMap});

  static Future<AppLocalizations> load(Locale locale) {
    final String localesPath = "assets/langs/" + locale.languageCode + ".json";

    return loadTranslations(localesPath).then((map) {
      return AppLocalizations(locale: locale, translationsMap: map);
    });
  }

  static Future<Map<String, String>> loadTranslations(String localesPath) async {
    Map<String, String> result = {};

    try {
      Map<String, dynamic> jsonContent = json.decode(await rootBundle.loadString(localesPath));
      jsonContent.forEach((key, value) => result.putIfAbsent(key, () => value as String));
    } catch (e, stack) {
      print("Failed while fetching translations from '$localesPath'." + e.toString() + "\n" + stack.toString());
    }

    return result;
  }

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  Map<String, String> translationsMap;

  String translate(final String key) {
    if (translationsMap.containsKey(key))
      return translationsMap[key];
    else
      return key;
  }
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  static const LocalizationsDelegate<AppLocalizations> delegate = AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return ['en', 'sr'].contains(locale.languageCode);
  }

  @override
  Future<AppLocalizations> load(Locale locale) {
    return AppLocalizations.load(locale);
  }

  @override
  bool shouldReload(LocalizationsDelegate<AppLocalizations> old) {
    return false;
  }
}