import 'package:flutter/material.dart';
import 'package:garduino_mobile/services/system_config.dart';
import '../../module/system_config.dart';
import 'package:flutter_picker/flutter_picker.dart';
import '../../util/public_extension.dart';


class PlantSystemConfig extends StatefulWidget {
  @override
  _PlantSystemConfigState createState() => _PlantSystemConfigState();
}

class _PlantSystemConfigState extends State<PlantSystemConfig> {
  final TextStyle textStyle =
      TextStyle(fontFamily: 'Montserrat', fontSize: 17.0);

  final TextStyle messageTextStyle = TextStyle(fontFamily: 'Montserrat', fontSize: 14.0);

  SystemConfigService systemConfigService = SystemConfigService();

  // config model with all params
  SystemConfigModel cachedConfigModel;

  String showMessage = null;

  Future<SystemConfigModel> _fetchConfig() {
    return systemConfigService.fetchConfig(context);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          resizeToAvoidBottomPadding: false,
          appBar: AppBar(
            title: Text("system-settings-title".tr(context), style: textStyle),
          ),
          body: Center(
              child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              (showMessage != null)
                  ? Padding(padding: EdgeInsets.all(20),child: Text(showMessage, style: messageTextStyle, textAlign: TextAlign.center,))
                  : Container(),
              getConfigs(),
              SizedBox(
                height: 10,
              ),
              updateConfigButton(),
            ],
          ))),
    );
  }

  bool updateInProgress = false;

  Widget updateConfigButton() {
    if (!updateInProgress)
      return RaisedButton(
          child: Text("update-configuration-btn".tr(context)),
          onPressed: () {
            var responseMessage;
            print("Update config pressed");
            systemConfigService
                .updateSystemConfig(context, cachedConfigModel)
                .then((onResponse) {
              setState(() {
                updateInProgress = false;
                showMessage = "settings-message-updated-success".tr(context);
              });
            }).catchError((onError) {
              setState(() {
                updateInProgress = false;
                showMessage = "settings-message-updated-error".tr(context) + onError.toString();
              });
            });

            setState(() {
              updateInProgress = true;
              showMessage = null;
            });
          });
    else {
      return CircularProgressIndicator();
    }
  }

  Widget getConfigs() {
    if (cachedConfigModel != null)
      return _systemConfigWidget(cachedConfigModel);
    else {
      return FutureBuilder<SystemConfigModel>(
        future: _fetchConfig(),
        builder: (context, AsyncSnapshot<SystemConfigModel> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          if (snapshot.hasError) {
            return Text("Error occured while fetching settings. " +
                snapshot.error.toString());
          }

          if (snapshot.hasData) {
            cachedConfigModel = snapshot.data;
            print("Config values: " + cachedConfigModel.toString());
            return _systemConfigWidget(snapshot.data);
          }

          return CircularProgressIndicator();
        },
      );
    }
  }

  Widget _systemConfigWidget(SystemConfigModel configModel) {
    return Padding(
        padding: EdgeInsets.only(left: 10, right: 10),
        child: Table(
          border: TableBorder.all(width: 0.4),
          columnWidths: {
            0: FlexColumnWidth(0.9)
          },
          defaultVerticalAlignment: TableCellVerticalAlignment.middle,
          children: [
            TableRow(children: [
              Text("settings-auto-watering".tr(context),
                  textAlign: TextAlign.center, style: textStyle),
              TableCell(
                child: Center(
                  child: Container(
                    width: 80,
                    child: Switch(
                      value: cachedConfigModel.autoWateringEnabled,
                      onChanged: (value) {
                        setState(() {
                          cachedConfigModel.autoWateringEnabled = value;
                        });
                      },
                    ),
                  ),
                ),
              ),
            ]),
            TableRow(children: [
              Text("settings-turn-led-during-watering".tr(context),
                  textAlign: TextAlign.center, style: textStyle),
              TableCell(
                child: Center(
                  child: Container(
                    width: 80,
                    child: Switch(
                      value: cachedConfigModel.lightOnDuringWatering,
                      onChanged: (value) {
                        setState(() {
                          cachedConfigModel.lightOnDuringWatering = value;
                        });
                      },
                    ),
                  ),
                ),
              ),
            ]),
            TableRow(children: [
              Text("settings-receive-notif".tr(context),
                  textAlign: TextAlign.center, style: textStyle),
              TableCell(
                child: Center(
                  child: Container(
                    width: 80,
                    child: Switch(
                      value: cachedConfigModel.telegramNotifierEnabled,
                      onChanged: (value) {
                        setState(() {
                          cachedConfigModel.telegramNotifierEnabled = value;
                        });
                      },
                    ),
                  ),
                ),
              ),
            ]),
            TableRow(children: [
              Text("settings-metrics-scraping-interval".tr(context),
                  textAlign: TextAlign.center, style: textStyle),
              TableCell(
                child: GestureDetector(
                    onTap: () {
                      Picker(
                        textStyle: textStyle,
                        adapter: PickerDataAdapter(
                          data: [
                            PickerItem(
                                children: [
                              for (int i = 1; i < 60; i++)
                                PickerItem(text: Text("$i"), value: i)
                            ]),
                            PickerItem(children: [
                              PickerItem(text: Text("second".tr(context)), value: "sec"),
                              PickerItem(text: Text("minute".tr(context)), value: "min")
                            ]),
                          ],
                          isArray: true,
                        ),
                        hideHeader: true,
                        title: new Text("settings-metrics-scraping-dialog".tr(context)),
                        onConfirm: (Picker picker, List value) {
                          print(picker.getSelectedValues()[0].toString() +
                              " " +
                              picker.getSelectedValues()[1]);

                          setState(() {
                            cachedConfigModel.scrapeInterval =
                                picker.getSelectedValues()[0].toString() +
                                    " " +
                                    picker.getSelectedValues()[1];
                          });
                        },
                      ).showDialog(context);
                    },
                    child: Text(
                      cachedConfigModel.getScrapeIntervalLocalized(context),
                      style: textStyle,
                      textAlign: TextAlign.center,
                    )),
              )
            ]),
            TableRow(children: [
              Text("settings-num-of-scrapes".tr(context),
                  textAlign: TextAlign.center, style: textStyle),
              TableCell(
                child: GestureDetector(
                    onTap: () {
                      Picker(
                        adapter: NumberPickerAdapter(
                          data: [
                            NumberPickerColumn(
                                initValue:
                                    int.parse(cachedConfigModel.numOfScrapes),
                                begin: 2,
                                end: 20),
                          ],
                        ),
                        hideHeader: true,
                        title: new Text("settings-num-of-scrapes-dialog".tr(context)),
                        onConfirm: (Picker picker, List value) {
                          print(picker.getSelectedValues()[0].toString());

                          setState(() {
                            cachedConfigModel.numOfScrapes =
                                picker.getSelectedValues()[0].toString();
                          });
                        },
                      ).showDialog(context);
                    },
                    child: Text(
                      cachedConfigModel.numOfScrapes,
                      style: textStyle,
                      textAlign: TextAlign.center,
                    )),
              )
            ]),
            TableRow(children: [
              Text("settings-soil-moisture".tr(context),
                  textAlign: TextAlign.center, style: textStyle),
              TableCell(
                child: GestureDetector(
                    onTap: () {
                      Picker(
                        adapter: NumberPickerAdapter(
                          data: [
                            NumberPickerColumn(
                                initValue: int.parse(
                                    cachedConfigModel.moistureThreshold),
                                begin: -1,
                                end: 100,
                                onFormatValue: (value) => (value >= 0) ? "$value %" : "do-not-set".tr(context)),
                          ],
                        ),
                        hideHeader: true,
                        title: new Text(
                            "settings-soil-moisture-dialog".tr(context)),
                        onConfirm: (Picker picker, List value) {
                          print("<" +
                              picker.getSelectedValues()[0].toString() +
                              "%");

                          setState(() {
                            cachedConfigModel.moistureThreshold =
                                picker.getSelectedValues()[0].toString();
                          });
                        },
                      ).showDialog(context);
                    },
                    child: Text(
    (cachedConfigModel.moistureThreshold != "-1") ? "<" + cachedConfigModel.moistureThreshold + "%" : "not-set-value".tr(context),
                      style: textStyle,
                      textAlign: TextAlign.center,
                    )),
              )
            ]),
            TableRow(children: [
              Text("settings-air-temperature".tr(context),
                  textAlign: TextAlign.center, style: textStyle),
              TableCell(
                child: GestureDetector(
                    onTap: () {
                      Picker(
                        adapter: NumberPickerAdapter(
                          data: [
                            NumberPickerColumn(
                                initValue: int.parse(
                                    cachedConfigModel.temperatureThreshold),
                                begin: -1,
                                end: 60,
                                onFormatValue: (value) => (value >= 0) ? "$value °C" : "do-not-set".tr(context)),
                          ],
                        ),
                        hideHeader: true,
                        title: new Text(
                            "settings-air-temperature-dialog".tr(context)),
                        onConfirm: (Picker picker, List value) {
                          print(">" +
                              picker.getSelectedValues()[0].toString() +
                              "°C");

                          setState(() {
                            cachedConfigModel.temperatureThreshold =
                                picker.getSelectedValues()[0].toString();
                          });
                        },
                      ).showDialog(context);
                    },
                    child: Text(
    (cachedConfigModel.temperatureThreshold != "-1") ? ">" + cachedConfigModel.temperatureThreshold + "°C" : "not-set-value".tr(context),
                      style: textStyle,
                      textAlign: TextAlign.center,
                    )),
              )
            ]),
            TableRow(children: [
              Text("settings-air-humidity".tr(context),
                  textAlign: TextAlign.center, style: textStyle),
              TableCell(
                child: GestureDetector(
                    onTap: () {
                      Picker(
                        adapter: NumberPickerAdapter(
                          data: [
                            NumberPickerColumn(
                                initValue: int.parse(
                                    cachedConfigModel.humidityThreshold),
                                begin: -1,
                                end: 100,
                                onFormatValue: (value) => (value >= 0) ? "$value %" : "do-not-set".tr(context)),
                          ],
                        ),
                        hideHeader: true,
                        title: new Text(
                            "settings-air-humidity-dialog".tr(context)),
                        onConfirm: (Picker picker, List value) {
                          print("<" +
                              picker.getSelectedValues()[0].toString() +
                              "%");

                          setState(() {
                            cachedConfigModel.humidityThreshold =
                                picker.getSelectedValues()[0].toString();
                          });
                        },
                      ).showDialog(context);
                    },
                    child: Text(
    (cachedConfigModel.humidityThreshold != "-1") ? "<" + cachedConfigModel.humidityThreshold + "%" : "not-set-value".tr(context),
                      style: textStyle,
                      textAlign: TextAlign.center,
                    )),
              )
            ]),
          ],
        ));
  }
}
