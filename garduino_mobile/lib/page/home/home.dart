import 'package:flutter/material.dart';
import 'package:garduino_mobile/page/home/water_plant_icon.dart';
import 'package:garduino_mobile/util/app_localization.dart';
import 'package:garduino_mobile/util/boolean_wrapper.dart';
import '../../inherited_data_provider.dart';
import 'package:garduino_mobile/page/home/plant_status.dart';
import 'dialog_settings.dart';
import '../../util/public_extension.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var _myController;
  BooleanWrapper _autoRefreshSwitch, _mockedDataSwitch;

  @override
  Widget build(BuildContext context) {
    _myController = TextEditingController(
        text: InheritedDataProvider.of(context).serverAddress);
    _autoRefreshSwitch =
        BooleanWrapper(InheritedDataProvider.of(context).autoRefresh);
    _mockedDataSwitch =
        BooleanWrapper(InheritedDataProvider.of(context).mockedData);

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("title".tr(context)),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () {
              return _showDialog();
            },
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          PlantWateringIcon(),
          Padding(padding: EdgeInsets.all(20), child: PlantStatus()),
          Padding(
              padding: EdgeInsets.all(20),
              child: RaisedButton(
                child: Text("change-system-settings-btn".tr(context)),
                onPressed: () {
                  Navigator.pushNamed(context, "/settings");
                },
              )),
        ],
      ),
    );
  }

  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("settings-title".tr(context)),
          contentPadding: EdgeInsets.all(20),
          content: SettingsDialogContent(
            addressController: _myController,
            autoRefreshController: _autoRefreshSwitch,
            mockedDataController: _mockedDataSwitch,
          ),
          actions: <Widget>[
            FlatButton(
              child: new Text("close-btn".tr(context)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text("OK"),
              onPressed: () {
                if (_myController.text != "")
                  InheritedDataProvider.of(context).serverAddress =
                      _myController.text;

                InheritedDataProvider.of(context).autoRefresh =
                    _autoRefreshSwitch.value;

                InheritedDataProvider.of(context).mockedData =
                    _mockedDataSwitch.value;

                Navigator.of(context).pop();
                setState(() {}); // refresh page
              },
            )
          ],
        );
      },
    );
  }

  @override
  void dispose() {
    _myController.dispose();
    super.dispose();
  }
}
