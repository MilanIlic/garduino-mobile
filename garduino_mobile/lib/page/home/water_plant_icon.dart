import 'package:flutter/material.dart';
import 'package:garduino_mobile/services/water_plant.dart';
import 'package:garduino_mobile/util/public_extension.dart';

class PlantWateringIcon extends StatefulWidget {
  WaterPlantService waterPlantService = WaterPlantService();

  @override
  _PlantWateringIconState createState() => _PlantWateringIconState();
}

class _PlantWateringIconState extends State<PlantWateringIcon> {
  bool iconButtonPressed = false;


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20),
      child: GestureDetector(
          onTapCancel: () {
            setState(() {
              iconButtonPressed = false;
            });
          },
          onTapDown: (TapDownDetails details) {
            setState(() {
              iconButtonPressed = true;
            });
            widget.waterPlantService.waterPlant(context).then(
                    (onResponse) {
                  Scaffold.of(context).showSnackBar(SnackBar(content: Text("snackbar-plant-watering-success".tr(context)), duration: Duration(seconds: 2),));
                }
            ).catchError(
                    (onError) {
                  Scaffold.of(context).showSnackBar(SnackBar(content: Text("snackbar-plant-watering-error".tr(context)), duration: Duration(seconds: 2),));
                }
            );
          },
          onTapUp: (TapUpDetails details) {
            setState(() {
              iconButtonPressed = false;
            });
          },
          child: Card(
              elevation: 10.0,
              child: (iconButtonPressed) ? Image.asset("assets/plant_watering.png") : Image.asset("assets/plant.png")
          )
      ),
    );
  }
}