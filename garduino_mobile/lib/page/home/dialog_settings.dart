import 'package:flutter/material.dart';
import 'package:garduino_mobile/util/boolean_wrapper.dart';
import '../../util/public_extension.dart';

class SettingsDialogContent extends StatefulWidget {

  var addressController;
  BooleanWrapper autoRefreshController, mockedDataController;

  @override
  _SettingsDialogContentState createState() => _SettingsDialogContentState();

  SettingsDialogContent({Key key, this.addressController, this.autoRefreshController, this.mockedDataController});
}

class _SettingsDialogContentState extends State<SettingsDialogContent> {

  @override
  Widget build(BuildContext context) {

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          children: <Widget>[
            Text("ip-address-label".tr(context) + ": "),
            Expanded(
                child: TextField(
                  controller: widget.addressController,
                ))
          ],
        ),
        Row(
          children: <Widget>[
            Text("auto-refresh-label".tr(context) + ": "),
            Container(
              width: 70,
              child: Switch(
                value: widget.autoRefreshController.value,
                onChanged: (value) {
                  setState(() {
                    widget.autoRefreshController.value = value;
                  });
                },
              ),
            )
          ],
        ),
        Row(
          children: <Widget>[
            Text("mocked-data-label".tr(context) + ": "),
            Container(
              width: 70,
              child: Switch(
                value: widget.mockedDataController.value,
                onChanged: (value) {
                  setState(() {
                    widget.mockedDataController.value = value;
                  });
                },
              ),
            )
          ],
        ),
      ],
    );
  }
}