import 'dart:async';
import 'package:flutter/material.dart';
import 'package:garduino_mobile/module/plant_status.dart';
import 'package:garduino_mobile/services/plant_status.dart';
import '../../util/public_extension.dart';
import '../../inherited_data_provider.dart';

class PlantStatus extends StatefulWidget {
  Timer autoRefreshTimer;

  @override
  _PlantStatusState createState() => _PlantStatusState();
}

class _PlantStatusState extends State<PlantStatus> {
  TextStyle textStyle =
      TextStyle(fontFamily: 'Montserrat', fontSize: 19.0);
  TextStyle timeTextStyle =
      TextStyle(fontFamily: 'Montserrat', fontSize: 16.0);

  PlantStatusService plantStatusService = PlantStatusService();

  @override
  Widget build(BuildContext context) {
    bool autoRefresh = InheritedDataProvider.of(context).autoRefresh;

    if (autoRefresh) {
      if (widget.autoRefreshTimer == null || !widget.autoRefreshTimer.isActive)
        widget.autoRefreshTimer =
            new Timer.periodic(Duration(seconds: 5), (Timer t) {
          if (InheritedDataProvider.of(context).autoRefresh)
            setState(() {});
          else
            t.cancel();
        });
    } else {
      if (widget.autoRefreshTimer != null && widget.autoRefreshTimer.isActive)
        widget.autoRefreshTimer.cancel();
    }

    return Column(
      children: <Widget>[
        Padding(
            padding: EdgeInsets.only(bottom: 15),
            child: Text("plant-status-title".tr(context), style: textStyle)),
        builder(autoRefresh),
        SizedBox(
          height: 20,
        ),
        RaisedButton(
          child: Text("refresh-status-btn".tr(context)),
          onPressed: () {
            setState(() {});
          },
        )
      ],
    );
  }

  Widget builder(bool autoRefresh) {
    return FutureBuilder<PlantStatusModel>(
      future: _fetchPlantStatus(context),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting &&
            !autoRefresh) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (snapshot.hasError) {
          return Center(
            child: Text(snapshot.error.toString()),
          );
        }
        if (snapshot.hasData) {
          return _plantStatus(snapshot.data);
        }

        return Center(child: Text("Error: " + snapshot.error.toString()));
      },
    );
  }

  Widget autoBuilder() {
    new Timer.periodic(Duration(seconds: 5), (Timer t) => setState(() {}));

    return FutureBuilder<PlantStatusModel>(
      future: _fetchPlantStatus(context),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Center(
            child: Text(snapshot.error.toString()),
          );
        }
        if (snapshot.hasData) {
          return _plantStatus(snapshot.data);
        }
      },
    );
  }

  Widget _plantStatus(PlantStatusModel plantStatus) {
    return Padding(
      padding: EdgeInsets.only(left: 3, right: 3),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text("status-air-temp-label".tr(context), style: textStyle),
                ],
              ),
              Column(
                children: <Widget>[
                  Text(
                      (plantStatus.airTemperature != "unknown")
                          ? plantStatus.airTemperature + "°C"
                          : "unknown".tr(context),
                      style: textStyle),
                ],
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Text("status-air-hum-label".tr(context), style: textStyle),
                ],
              ),
              Column(
                children: <Widget>[
                  Text(
                      (plantStatus.airHumidity != "unknown")
                          ? plantStatus.airHumidity + "%"
                          : "unknown".tr(context),
                      style: textStyle),
                ],
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Text("status-soil-moisture-label".tr(context),
                      style: textStyle),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                      (plantStatus.soilMoisture != "unknown")
                          ? plantStatus.soilMoisture + "%"
                          : "unknown".tr(context),
                      style: textStyle),
                ],
              )
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Text("status-latest-status-label".tr(context),
                      style: textStyle),
                ],
              ),
              Column(
                children: <Widget>[
                  Text(plantStatus.timestamp.tr(context), style: timeTextStyle),
                ],
              )
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Text("status-latest-watering-label".tr(context),
                      style: textStyle),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(plantStatus.wateringDoneAt.tr(context),
                      style: timeTextStyle),
                ],
              )
            ],
          )
        ],
      ),
    );
  }

  Future<PlantStatusModel> _fetchPlantStatus(BuildContext context) async {
    return plantStatusService.fetchStatus(context);
  }
}
